# Copyright (c) Microsoft. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.

import random
import time

# Using the Python Device SDK for IoT Hub:
#   https://github.com/Azure/azure-iot-sdk-python
# The sample connects to a device-specific MQTT endpoint on your IoT Hub.
from azure.iot.device import IoTHubDeviceClient, Message

# The device connection string to authenticate the device with your IoT hub.
# Using the Azure CLI:
# az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table

#Alexis
#CONNECTION_STRING = "HostName=AlexisDEVL-IoThub.azure-devices.net;DeviceId=MyPythonDevice;SharedAccessKey=bxzpRdIyrXvnaAyDwMMJL3RAlv5j51KDbXbCmw8QxhA="

#Paul
#CONNECTION_STRING = "HostName=IotHubG3.azure-devices.net;DeviceId=MyCDevice;SharedAccessKey=qErLj94zMXVB6yyRcGOSM15fYqUDiPQ3vBPRlJY/1Nc="

#martin
CONNECTION_STRING = "HostName=mj1Hub.azure-devices.net;DeviceId=SENSOR1234;SharedAccessKey=wxoK7v40yPPiv7XNm39Eg6FpY0NHFr7MoECsXNv2uEU="

# Define the JSON message to send to IoT Hub.
TEMPERATURE = 20.0
HUMIDITY = 60
SMOG = 100
MSG_TXT = '{{"temperature": {temperature}, "humidity": {humidity}, "eCO2": {eco2}, "TVOC": {tvoc}, "GPS": [43.548818, 1.453652], "Battery": 67}}'


def iothub_client_init():
    # Create an IoT Hub client
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    return client


def iothub_client_telemetry_sample_run():
    try:
        client = iothub_client_init()
        print("IoT Hub device sending periodic messages, press Ctrl-C to exit")

        while True:
            # Build the message with simulated telemetry values.
            temperature = TEMPERATURE + (random.random() * 15)
            humidity = HUMIDITY + (random.random() * 20)
            eco2 = SMOG + (random.randint(100, 2000))
            tvoc = SMOG + (random.randint(100, 2000))
            msg_txt_formatted = MSG_TXT.format(temperature=temperature, humidity=humidity, eco2=eco2, tvoc=tvoc)
            message = Message(msg_txt_formatted)

            # Add a custom application property to the message.
            # An IoT hub can filter on these properties without access to the message body.
            if temperature > 30:
                message.custom_properties["temperatureAlert"] = "true"
            else:
                message.custom_properties["temperatureAlert"] = "false"

            # Send the message.
            print("Sending message: {}".format(message))
            client.send_message(message)
            print("Message successfully sent")
            time.sleep(300)

    except KeyboardInterrupt:
        print("IoTHubClient sample stopped")


if __name__ == '__main__':
    print("IoT Hub Quickstart #1 - Simulated device")
    print("Press Ctrl-C to exit")
    iothub_client_telemetry_sample_run()
