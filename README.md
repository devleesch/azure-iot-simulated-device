# azure-iot-simulated-device

## required resource

- Python 3.6
- internet access

### install azure iot library for python 
```bash
pip install azure-iot-device
```

### get your IoT hub device connection string

```bash
az iot hub monitor-events --hub-name {YourIoTHubName} --device-id MyPythonDevice 
```